package com.example.anil.switchmap_99;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view);

//        final RepoViewModel viewModel = ViewModelProviders.of(this).get(RepoViewModel.class);

        SingletonRepoClass viewModel = SingletonRepoClass.getInstance();

        viewModel.liveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(Main2Activity.this, s , Toast.LENGTH_SHORT).show();
                textView.setText(s);
                //viewModel.liveDataActivity1.setValue("hi asjlfjls");
            }
        });

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.className.setValue("activity2");
            }
        });

        findViewById(R.id.go_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(Main2Activity.this, MainActivity.class));

            }
        });
    }
}
