package com.example.anil.switchmap_99;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

public class RepoViewModel extends ViewModel {

    public MutableLiveData<String> liveDataActivity1 = new MutableLiveData<>();
    public MutableLiveData<String> liveDataActivity2 = new MutableLiveData<>();

    public MutableLiveData<String> className = new MutableLiveData<>();

    LiveData<String> liveData = Transformations.switchMap(className, (name) -> getDataByClassName(name));

    private LiveData<String> getDataByClassName(String name) {
        if(name.equals("activity1"))
        {
            liveDataActivity1.postValue("this is for activity 1");
            return liveDataActivity1;
        }
        else if (name.equals("activity2"))
        {
            liveDataActivity2.postValue("this is for activity 2");
            return liveDataActivity2;
        }
        else
            return null;
    }
}
