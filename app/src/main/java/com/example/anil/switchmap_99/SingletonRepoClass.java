package com.example.anil.switchmap_99;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

public class SingletonRepoClass {

   static SingletonRepoClass mInstance;

    public MutableLiveData<String> liveDataActivity1 = new MutableLiveData<>();
    public MutableLiveData<String> liveDataActivity2 = new MutableLiveData<>();

    public MutableLiveData<String> className = new MutableLiveData<>();

    LiveData<String> liveData = Transformations.switchMap(className, (name) -> getDataByClassName(name));

    private LiveData<String> getDataByClassName(String name) {
        if(name.equals("activity1"))
        {
            liveDataActivity1.postValue("this is for activity 1");
            return liveDataActivity1;
        }
        else if (name.equals("activity2"))
        {
            liveDataActivity2.postValue("this is for activity 2");
            return liveDataActivity2;
        }
        else
            return null;
    }

    public static synchronized SingletonRepoClass getInstance()
    {
        if(mInstance == null)
        {
            mInstance = new SingletonRepoClass();
        }
        return mInstance;
    }


}
